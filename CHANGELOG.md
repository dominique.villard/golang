## [2.1.2](https://gitlab.com/to-be-continuous/golang/compare/2.1.1...2.1.2) (2022-01-07)


### Bug Fixes

* Suffix version for go install command ([40065b4](https://gitlab.com/to-be-continuous/golang/commit/40065b4a7f8fd699a36c160d73634359f2dd453f))

## [2.1.1](https://gitlab.com/to-be-continuous/golang/compare/2.1.0...2.1.1) (2022-01-06)


### Bug Fixes

* use go install instead of deprecated go get ([91a5c14](https://gitlab.com/to-be-continuous/golang/commit/91a5c14c8ba6bb5bff0e484a4507297b2292b8e8))

# [2.1.0](https://gitlab.com/to-be-continuous/golang/compare/2.0.3...2.1.0) (2021-11-29)


### Features

* Add GO_PROJECT_DIR variable ([ce66c08](https://gitlab.com/to-be-continuous/golang/commit/ce66c088f281363fa12989ab8a6a8563d00c3196))

## [2.0.3](https://gitlab.com/to-be-continuous/golang/compare/2.0.2...2.0.3) (2021-11-27)


### Bug Fixes

* add missing test packages for sonar test run ([89f5881](https://gitlab.com/to-be-continuous/golang/commit/89f58815118b07c2f945177969910f9c426a7b08))

## [2.0.2](https://gitlab.com/to-be-continuous/golang/compare/2.0.1...2.0.2) (2021-11-26)


### Bug Fixes

* go test packages var typo ([041c464](https://gitlab.com/to-be-continuous/golang/commit/041c4644a7b14969ad592c096e1a68fd8bc01508))

## [2.0.1](https://gitlab.com/to-be-continuous/golang/compare/2.0.0...2.0.1) (2021-11-26)


### Bug Fixes

* sonar test report ([e0da931](https://gitlab.com/to-be-continuous/golang/commit/e0da93146900b1c2bb0270dbf5d24a195c7b9c9c))

# [2.0.0](https://gitlab.com/to-be-continuous/golang/compare/1.3.0...2.0.0) (2021-11-23)


### Features

* **build:** build target platform can be specified ([b068c40](https://gitlab.com/to-be-continuous/golang/commit/b068c40e5095bb69eb743832fb10e1cdb04d826a))


### BREAKING CHANGES

* **build:** changed `GO_BUILD_ARGS` to `GO_BUILD_FLAGS` variable and `GO_TEST_ARGS` to `GO_TEST_FLAGS` variable. See doc.

# [1.3.0](https://gitlab.com/to-be-continuous/golang/compare/1.2.2...1.3.0) (2021-11-21)


### Features

* make lint job auto on feature branches ([88324d9](https://gitlab.com/to-be-continuous/golang/commit/88324d9d60a60abaf50a62dc4ce0e4b93e20d566))

## [1.2.2](https://gitlab.com/to-be-continuous/golang/compare/1.2.1...1.2.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([758162a](https://gitlab.com/to-be-continuous/golang/commit/758162a50c91d21eda516f5c9930980b410ab7fe))

## [1.2.1](https://gitlab.com/to-be-continuous/golang/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([835c547](https://gitlab.com/to-be-continuous/golang/commit/835c5477a4d8e596b344f0806e918c1b1bc4d67c))

## [1.2.0](https://gitlab.com/to-be-continuous/golang/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([368e5a2](https://gitlab.com/to-be-continuous/golang/commit/368e5a2fbbe61bbe5bffeaa6c4bfcea101f8c47b))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/golang/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([cb75be4](https://gitlab.com/Orange-OpenSource/tbc/golang/commit/cb75be4aebdda6c510f8ea2af76308078db692d9))

## 1.0.0 (2021-05-06)

### Features

* initial release ([5518d12](https://gitlab.com/Orange-OpenSource/tbc/golang/commit/5518d126a81b2b8feb529b958868942f4b1bf900))
